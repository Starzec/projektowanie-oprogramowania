package com.example.demo.service;

import com.example.demo.DTO.TrasaDto;
import com.example.demo.model.Pasmo;
import com.example.demo.model.StatusTrasy;
import com.example.demo.model.TrasaZdefiniowana;
import com.example.demo.repository.TrasaZdefiniowanaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TrasaServiceTest {

    @InjectMocks
    private TrasaService trasaService;

    @Mock
    private TrasaZdefiniowanaRepository trasaZdefiniowanaRepository;

    private List<TrasaZdefiniowana> trasy = new ArrayList<>();

    private Pasmo pasmo = new Pasmo(1L, "pasmo1", null);

    private StatusTrasy statusTrasy = new StatusTrasy("Status");

    @BeforeEach
    void setUp() throws Exception {
        trasy.add(new TrasaZdefiniowana(4L, LocalDate.of(2014, 3, 3), "TrasaZdefiniowana", pasmo, statusTrasy, 8, null, null));
        trasy.add(new TrasaZdefiniowana(6L, LocalDate.of(2014, 3, 3), "TrasaZdefiniowana", pasmo, statusTrasy, 8, null, null));
        trasy.add(new TrasaZdefiniowana(7L, LocalDate.of(2014, 3, 3), "TrasaZdefiniowana", pasmo, statusTrasy, 8, null, null));
    }

    @Test
    void findAllDefinedTracesInRegion() {
        when(trasaZdefiniowanaRepository.findAllByPasmoPasmoIdAndStatusTrasyStatusTrasy(anyLong(), anyString())).thenReturn(trasy);

        List<TrasaDto> response = trasaService.findAllDefinedTracesInRegion(1L);

        List<TrasaDto> correct = trasy.stream()
                .map(trace -> new TrasaDto(trace.getTrasaId(), trace.getDataUtworzenia().toString(), trace.getNazwa(), trace.getPasmo(),
                        trace.getStatusTrasy().getStatusTrasy(), trace.getPunkty(), trace.getPunktPoczatkowy(), trace.getPunktKoncowy()))
                .collect(Collectors.toList());

        assertThat(response).hasSize(correct.size()).containsExactlyElementsOf(correct);
    }

    @Test
    void findAllDefinedTracesInRegionPasmoDoesntExist() {
        List<TrasaDto> response = trasaService.findAllDefinedTracesInRegion(1L);

        assertThat(response).isEmpty();
    }
}