package com.example.demo.service;

import com.example.demo.DTO.OdznakaDto;
import com.example.demo.DTO.TypOdznakiDto;
import com.example.demo.exception.OdznakaExistsException;
import com.example.demo.exception.ZleDaneException;
import com.example.demo.model.Odznaka;
import com.example.demo.model.Turysta;
import com.example.demo.model.TypOdznaki;
import com.example.demo.model.TypOdznakiTurystaKey;
import com.example.demo.repository.OdznakaRepository;
import com.example.demo.repository.TurystaRepository;
import com.example.demo.repository.TypOdznakiRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OdznakaServiceTest {

    @InjectMocks
    private OdznakaService odznakaService;

    @Mock
    private OdznakaRepository odznakaRepository;

    @Mock
    private TurystaRepository turystaRepository;

    @Mock
    private TypOdznakiRepository typOdznakiRepository;


    private TypOdznaki typ = new TypOdznaki(1L, 20, "W gory", "some url");

    private TypOdznaki typ2 = new TypOdznaki(2L, 50, "Popularna", "some url");

    private TypOdznaki typ3 = new TypOdznaki(3L, 70, "Mala brazowa", "some url");

    private Turysta tur = new Turysta(LocalDate.of(2014, 3, 3));

    //findAllObtainedBadges BEGINS

    @Test
    void findAllObtainedBadges() throws ZleDaneException {
        List<Odznaka> odznaki = List.of(new Odznaka(new TypOdznakiTurystaKey(1L, 1L), typ, tur, true, null, 0),
                new Odznaka(new TypOdznakiTurystaKey(2L, 1L), typ2, tur, true, null, 0),
                new Odznaka(new TypOdznakiTurystaKey(3L, 1L), typ3, tur, false, null, 0));
        when(turystaRepository.findById(anyLong())).thenReturn(Optional.of(tur));
        when(odznakaRepository.findAllByTurysta(any())).thenReturn(odznaki);

        List<OdznakaDto> response = odznakaService.findAllObtainedBadges(1L);

        List<OdznakaDto> correct = odznaki.stream()
                .map(odznaka -> new OdznakaDto(odznaka.getTypOdznaki(), odznaka.getTurysta(), odznaka.getCzyZdobyta(), odznaka.getRokZdobycia(), odznaka.getAktualnePunkty()))
                .collect(Collectors.toList());

        assertThat(response).hasSize(correct.size()).containsExactlyElementsOf(correct);
    }

    @Test
    void findAllObtainedBadgesTurystaDoesntExist(){
        assertThrows(ZleDaneException.class, () -> odznakaService.findAllObtainedBadges(1L));
    }

    @Test
    void findAllObtainedBadgesNoBadges() throws ZleDaneException {
        when(turystaRepository.findById(anyLong())).thenReturn(Optional.of(tur));

        List<OdznakaDto> response = odznakaService.findAllObtainedBadges(1L);

        assertThat(response).isEmpty();
    }
    //ENDS

    //addTypOdznaki BEGINS

    @Test
    void addTypOdznaki() throws OdznakaExistsException, ZleDaneException {
        when(typOdznakiRepository.existsByNazwa(anyString())).thenReturn(false);

        odznakaService.addTypOdznaki(TypOdznakiDto.toTypOdznakiDto(typ));

        verify(typOdznakiRepository, times(1)).save(any());
    }

    @Test
    void addTypOdznakiTypExists() {
        when(typOdznakiRepository.existsByNazwa(anyString())).thenReturn(true);

        assertThrows(OdznakaExistsException.class, () -> odznakaService.addTypOdznaki(TypOdznakiDto.toTypOdznakiDto(typ)));

        verify(typOdznakiRepository, never()).save(any());
    }

    @Test
    void addTypOdznakiEmptyNullValues(){
        when(typOdznakiRepository.existsByNazwa(anyString())).thenReturn(false);

        String oldNazwa = typ.getNazwa();
        typ.setNazwa("");
        assertThrows(ZleDaneException.class, () -> odznakaService.addTypOdznaki(TypOdznakiDto.toTypOdznakiDto(typ)));
        typ.setNazwa(null);
        assertThrows(ZleDaneException.class, () -> odznakaService.addTypOdznaki(TypOdznakiDto.toTypOdznakiDto(typ)));
        typ.setNazwa(oldNazwa);
        String oldZdjecie = typ.getZdjecie();
        typ.setZdjecie("");
        assertThrows(ZleDaneException.class, () -> odznakaService.addTypOdznaki(TypOdznakiDto.toTypOdznakiDto(typ)));
        typ.setZdjecie(null);
        assertThrows(ZleDaneException.class, () -> odznakaService.addTypOdznaki(TypOdznakiDto.toTypOdznakiDto(typ)));
        typ.setZdjecie(oldZdjecie);
        typ.setWymaganePunkty(null);
        assertThrows(ZleDaneException.class, () -> odznakaService.addTypOdznaki(TypOdznakiDto.toTypOdznakiDto(typ)));

        verify(typOdznakiRepository, never()).save(any());
    }
    //ENDS
}