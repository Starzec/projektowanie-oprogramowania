package com.example.demo.service;

import com.example.demo.DTO.WycieczkaDto;
import com.example.demo.exception.OdznakaExistsException;
import com.example.demo.exception.WycieczkaNotFoundException;
import com.example.demo.exception.ZleDaneException;
import com.example.demo.model.*;
import com.example.demo.repository.OdznakaRepository;
import com.example.demo.repository.TurystaRepository;
import com.example.demo.repository.TypOdznakiRepository;
import com.example.demo.repository.WycieczkaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class WycieczkaServiceTest {

    @InjectMocks
    private WycieczkaService wycieczkaService;

    @Mock
    private WycieczkaRepository wycieczkaRepository;

    @Mock
    private OdznakaRepository odznakaRepository;

    @Mock
    private TurystaRepository turystaRepository;

    @Mock
    private TypOdznakiRepository typOdznakiRepository;

    private Wycieczka wycieczka;

    private Odznaka odznaka;

    @BeforeEach
    void setUp() {
        Turysta tur = new Turysta(LocalDate.of(2014, 3, 3));
        TypOdznaki typ = new TypOdznaki(1L, 20, "W gory", "some url");
        odznaka = new Odznaka(new TypOdznakiTurystaKey(1L, 1L), typ, tur, true, null, 0);
        wycieczka = new Wycieczka(1L, LocalDate.of(2014, 3, 3), 12, "Dsa", true, false, odznaka, new ArrayList<>());

    }

    //verifyWycieczka BEGINS

    @Test
    void verifyWycieczkaCorrectData() throws OdznakaExistsException, ZleDaneException, WycieczkaNotFoundException {
        when(wycieczkaRepository.findByWycieczkaId(anyLong())).thenReturn(Optional.of(wycieczka));
        when(odznakaRepository.findOdznakaByTurystaAndTypOdznaki(any(), any())).thenReturn(Optional.of(odznaka));

        boolean oldZweryfikowana = wycieczka.getZweryfikowana();
        boolean oldDoWeryfikacji = wycieczka.getDoWeryfikacji();
        Integer punkty = odznaka.getAktualnePunkty();

        wycieczkaService.verifyWycieczka(1L);

        assertThat(punkty).isLessThan(odznaka.getAktualnePunkty());
        assertTrue(wycieczka.getZweryfikowana());
        assertFalse(wycieczka.getDoWeryfikacji());
        assertNotEquals(oldZweryfikowana, wycieczka.getZweryfikowana());
        assertNotEquals(oldDoWeryfikacji, wycieczka.getDoWeryfikacji());
        verify(wycieczkaRepository, times(1)).save(any());
        verify(odznakaRepository, times(1)).save(any());
    }

    @Test
    void verifyWycieczkaVerifiedAlready() {
        wycieczka.setZweryfikowana(true);
        when(wycieczkaRepository.findByWycieczkaId(anyLong())).thenReturn(Optional.of(wycieczka));

        boolean oldZweryfikowana = wycieczka.getZweryfikowana();
        boolean oldDoWeryfikacji = wycieczka.getDoWeryfikacji();
        Integer punkty = odznaka.getAktualnePunkty();

        assertThrows(WycieczkaNotFoundException.class, () -> wycieczkaService.verifyWycieczka(1L));

        assertEquals(punkty, odznaka.getAktualnePunkty());
        assertEquals(oldZweryfikowana, wycieczka.getZweryfikowana());
        assertEquals(oldDoWeryfikacji, wycieczka.getDoWeryfikacji());
        verify(wycieczkaRepository, never()).save(any());
        verify(odznakaRepository, never()).save(any());
    }

    @Test
    void verifyWycieczkaDoesntExist(){
        assertThrows(WycieczkaNotFoundException.class, () -> wycieczkaService.verifyWycieczka(1L));
        verify(wycieczkaRepository, never()).save(any());
        verify(odznakaRepository, never()).save(any());
    }

    @Test
    void verifyWycieczkaOdznakaDoesntExist(){
        when(wycieczkaRepository.findByWycieczkaId(anyLong())).thenReturn(Optional.of(wycieczka));

        boolean oldZweryfikowana = wycieczka.getZweryfikowana();
        boolean oldDoWeryfikacji = wycieczka.getDoWeryfikacji();
        Integer punkty = odznaka.getAktualnePunkty();

        assertThrows(OdznakaExistsException.class, () -> wycieczkaService.verifyWycieczka(1L));

        assertEquals(punkty, odznaka.getAktualnePunkty());
        assertEquals(oldZweryfikowana, wycieczka.getZweryfikowana());
        assertEquals(oldDoWeryfikacji, wycieczka.getDoWeryfikacji());
        verify(wycieczkaRepository, never()).save(any());
        verify(odznakaRepository, never()).save(any());
    }

    @Test
    void verifyWycieczkaIsNotToVerify(){
        wycieczka.setDoWeryfikacji(false);
        when(wycieczkaRepository.findByWycieczkaId(anyLong())).thenReturn(Optional.of(wycieczka));

        boolean oldZweryfikowana = wycieczka.getZweryfikowana();
        boolean oldDoWeryfikacji = wycieczka.getDoWeryfikacji();
        Integer punkty = odznaka.getAktualnePunkty();

        assertThrows(WycieczkaNotFoundException.class, () -> wycieczkaService.verifyWycieczka(1L));

        assertEquals(punkty, odznaka.getAktualnePunkty());
        assertEquals(oldZweryfikowana, wycieczka.getZweryfikowana());
        assertEquals(oldDoWeryfikacji, wycieczka.getDoWeryfikacji());
        verify(wycieczkaRepository, never()).save(any());
        verify(odznakaRepository, never()).save(any());
    }

    //ENDS

    //findAllWycieczkiDoWeryfikacji BEGINS

    private List<Wycieczka> createListWycieczka(){
        return List.of(new Wycieczka(1L, LocalDate.of(2014, 3, 3), 12, "Dsa", true, false, odznaka, null),
                new Wycieczka(2L, LocalDate.of(2014, 3, 3), 12, "Dsa", true, true, odznaka, null),
                new Wycieczka(3L, LocalDate.of(2014, 3, 3), 12, "Dsa", false, false, odznaka, null),
                new Wycieczka(4L, LocalDate.of(2014, 3, 3), 12, "Dsa", false, true, odznaka, null),
                new Wycieczka(5L, LocalDate.of(2014, 3, 3), 12, "Dsa", true, false, odznaka, null));
    }

    @Test
    void findAllWycieczkiDoWeryfikacjiCorrectData(){
        List<Wycieczka> wycieczki = createListWycieczka();
        List<Wycieczka> doWeryfikacjiTrue = wycieczki.stream().
                filter(Wycieczka::getDoWeryfikacji).
                collect(Collectors.toList());
        when(wycieczkaRepository.findByDoWeryfikacjiTrue()).thenReturn(doWeryfikacjiTrue);

        List<WycieczkaDto> response = wycieczkaService.findAllWycieczkiDoWeryfikacji();

        List<WycieczkaDto> correct = doWeryfikacjiTrue.stream().
                filter(w -> !w.getZweryfikowana())
                .map(WycieczkaDto::toWycieczkaDto)
                .collect(Collectors.toList());

        assertThat(response).hasSize(correct.size()).containsExactlyElementsOf(correct);
    }

    @Test
    void findAllWycieczkiDoWeryfikacjiEmpty(){
        List<WycieczkaDto> response = wycieczkaService.findAllWycieczkiDoWeryfikacji();

        assertThat(response).isEmpty();
    }

    //ENDS
}