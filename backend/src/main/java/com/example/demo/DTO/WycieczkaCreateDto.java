package com.example.demo.DTO;

import com.example.demo.model.Odznaka;
import com.example.demo.model.Trasa;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class WycieczkaCreateDto {

    private String nazwa;

    private Odznaka odznaka;

    private List<Trasa> trasaList;

    private LocalDate date;
}
