package com.example.demo.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TrasaWlasnaDto {

    private Integer roznicaWysokosci;

    private Integer dlugosc;

    private Integer punktyZaPrzejscie;
}
