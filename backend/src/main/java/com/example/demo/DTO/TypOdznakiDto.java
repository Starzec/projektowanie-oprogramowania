package com.example.demo.DTO;

import com.example.demo.model.TypOdznaki;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class TypOdznakiDto {

    private Long typOdznakiId;

    private Integer wymaganePunkty;

    private String nazwa;

    private String zdjecie;

    public static TypOdznakiDto toTypOdznakiDto(TypOdznaki typOdznaki){
        return TypOdznakiDto.builder()
                .typOdznakiId(typOdznaki.getTypOdznakiId())
                .wymaganePunkty(typOdznaki.getWymaganePunkty())
                .nazwa(typOdznaki.getNazwa())
                .zdjecie(typOdznaki.getZdjecie())
                .build();
    }
}
