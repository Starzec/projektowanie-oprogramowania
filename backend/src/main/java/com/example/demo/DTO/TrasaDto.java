package com.example.demo.DTO;

import com.example.demo.model.Pasmo;
import com.example.demo.model.Punkt;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TrasaDto {

    private Long trasaId;

    private String dataUtworzenia;

    private String nazwa;

    private Pasmo pasmo;

    private String statusTrasy;

    private Integer punkty;

    private Punkt punktPoczatkowy;

    private Punkt punktKoncowy;
}
