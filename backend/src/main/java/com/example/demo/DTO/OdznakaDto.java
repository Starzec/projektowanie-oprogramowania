package com.example.demo.DTO;

import com.example.demo.model.Odznaka;
import com.example.demo.model.Turysta;
import com.example.demo.model.TypOdznaki;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class OdznakaDto {

    private TypOdznaki typOdznaki;

    private Turysta turysta;

    private Boolean czyZdobyta;

    private LocalDate rokZdobycia;

    private Integer aktualnePunkty;

    public static OdznakaDto toOdznakaDto(Odznaka odznaka){
        return OdznakaDto.builder()
                .typOdznaki(odznaka.getTypOdznaki())
                .turysta(odznaka.getTurysta())
                .build();
    }
}
