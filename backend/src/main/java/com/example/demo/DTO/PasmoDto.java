package com.example.demo.DTO;

import com.example.demo.model.GrupaGorska;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PasmoDto {

    private Long pasmoId;

    private String nazwa;

    private GrupaGorska grupaGorska;
}
