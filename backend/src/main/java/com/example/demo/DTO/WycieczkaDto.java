package com.example.demo.DTO;

import com.example.demo.model.Odznaka;
import com.example.demo.model.Trasa;
import com.example.demo.model.Wycieczka;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class WycieczkaDto {

    private Long wycieczkaId;

    private String dataWycieczki;

    private Integer punktyZaWycieczke;

    private String nazwa;

    private Boolean doWeryfikacji;

    private Boolean zweryfikowana;

    private Odznaka odznaka;

    private List<Trasa> trasaList;

    public static WycieczkaDto toWycieczkaDto(Wycieczka wycieczka){
        return WycieczkaDto.builder()
                .wycieczkaId(wycieczka.getWycieczkaId())
                .dataWycieczki(wycieczka.getDataWycieczki().toString())
                .punktyZaWycieczke(wycieczka.getPunktyZaWycieczke())
                .nazwa(wycieczka.getNazwa())
                .doWeryfikacji(wycieczka.getDoWeryfikacji())
                .zweryfikowana(wycieczka.getZweryfikowana())
                .odznaka(wycieczka.getOdznaka())
                .trasaList(wycieczka.getTrasaList())
                .build();
    }
}
