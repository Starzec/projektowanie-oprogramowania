package com.example.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({WycieczkaNotFoundException.class, OdznakaExistsException.class})
    public ResponseEntity handleNotFound(Exception ex){
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
    }

    @ExceptionHandler(ZleDaneException.class)
    public ResponseEntity hanldeBadRequest(Exception ex){
        return ResponseEntity.badRequest().body(ex.getMessage());
    }
}
