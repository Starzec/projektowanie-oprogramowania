package com.example.demo.service;

import com.example.demo.DTO.PasmoDto;
import com.example.demo.repository.PasmoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PasmoService {

    private final PasmoRepository pasmoRepository;

    public List<PasmoDto> findPasmaWithGrupa(Long grupaId) {
        return pasmoRepository.findByGrupaGorskaGrupaGorskaId(grupaId).stream()
                .map(pasmo -> new PasmoDto(pasmo.getPasmoId(), pasmo.getNazwa(), pasmo.getGrupaGorska()))
                .collect(Collectors.toList());
    }
}
