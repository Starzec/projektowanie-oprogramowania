package com.example.demo.service;

import com.example.demo.DTO.OdznakaDto;
import com.example.demo.DTO.TypOdznakiDto;
import com.example.demo.exception.OdznakaExistsException;
import com.example.demo.exception.ZleDaneException;
import com.example.demo.model.Turysta;
import com.example.demo.model.TypOdznaki;
import com.example.demo.repository.OdznakaRepository;
import com.example.demo.repository.TurystaRepository;
import com.example.demo.repository.TypOdznakiRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class OdznakaService {

    private final OdznakaRepository odznakaRepository;

    private final TurystaRepository turystaRepository;  // Gdyby było logowanie to niepotrzebne

    private final TypOdznakiRepository typOdznakiRepository;

    public List<OdznakaDto> findAllObtainedBadges(Long turystaId) throws ZleDaneException {
        turystaId = 1L;
        Optional<Turysta> turysta = turystaRepository.findById(turystaId);
        if(turysta.isPresent())
            return odznakaRepository.findAllByTurysta(turysta.get()).stream()
                    .map(odznaka -> new OdznakaDto(odznaka.getTypOdznaki(), odznaka.getTurysta(), odznaka.getCzyZdobyta(), odznaka.getRokZdobycia(), odznaka.getAktualnePunkty()))
                    .collect(Collectors.toList());
        else
            throw new ZleDaneException("Turysta nie znaleziony");
    }

    public List<TypOdznakiDto> findAllTypOdznaki() {
        return typOdznakiRepository.findAll().stream()
                .map(TypOdznakiDto::toTypOdznakiDto)
                .collect(Collectors.toList());
    }

    public void addTypOdznaki(TypOdznakiDto typOdznakiDto) throws OdznakaExistsException, ZleDaneException {
        if(!typOdznakiRepository.existsByNazwa(typOdznakiDto.getNazwa())){
            if(typOdznakiDto.getWymaganePunkty() == null || typOdznakiDto.getNazwa() == null || typOdznakiDto.getNazwa().equals("")
                    || typOdznakiDto.getZdjecie() == null || typOdznakiDto.getZdjecie().equals(""))
                throw new ZleDaneException("Dane nie moga byc puste");
            TypOdznaki typ = TypOdznaki.builder()
                    .nazwa(typOdznakiDto.getNazwa())
                    .wymaganePunkty(typOdznakiDto.getWymaganePunkty())
                    .zdjecie(typOdznakiDto.getZdjecie())
                    .build();
            typOdznakiRepository.save(typ);
        }
        else
            throw new OdznakaExistsException("Taki typ odznaki istnieje");
    }
}
