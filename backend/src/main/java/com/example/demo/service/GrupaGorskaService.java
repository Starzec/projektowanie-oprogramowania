package com.example.demo.service;

import com.example.demo.DTO.GrupaGorskaDto;
import com.example.demo.repository.GrupaGorskaRepository;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GrupaGorskaService {

    private final GrupaGorskaRepository grupaGorskaRepository;

    public List<GrupaGorskaDto> findAllGrupyGorskie() {
        return Lists.newArrayList((grupaGorskaRepository.findAll())).stream()
                .map(grupa -> new GrupaGorskaDto(grupa.getGrupaGorskaId(), grupa.getNazwa()))
                .collect(Collectors.toList());
    }
}
