package com.example.demo.service;

import com.example.demo.DTO.TrasaDto;
import com.example.demo.repository.TrasaZdefiniowanaRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class TrasaService {

    private final TrasaZdefiniowanaRepository trasaZdefiniowanaRepository;

    public static final String ACTIVE_TRACE = "aktywna";

    public static final String INACTIVE_TRACE = "nieaktywna";

    public static final String DELETED_TRACE = "usunieta";

    public List<TrasaDto> findAllDefinedTracesInRegion(Long pasmoId) {
        return trasaZdefiniowanaRepository.findAllByPasmoPasmoIdAndStatusTrasyStatusTrasy(pasmoId, ACTIVE_TRACE).stream()
                .map(trace -> new TrasaDto(trace.getTrasaId(), trace.getDataUtworzenia().toString(), trace.getNazwa(), trace.getPasmo(),
                        trace.getStatusTrasy().getStatusTrasy(), trace.getPunkty(), trace.getPunktPoczatkowy(), trace.getPunktKoncowy()))
                .collect(Collectors.toList());
    }
}
