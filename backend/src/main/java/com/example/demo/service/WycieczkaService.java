package com.example.demo.service;

import com.example.demo.DTO.WycieczkaCreateDto;
import com.example.demo.DTO.WycieczkaDto;
import com.example.demo.exception.OdznakaExistsException;
import com.example.demo.exception.WycieczkaNotFoundException;
import com.example.demo.exception.ZleDaneException;
import com.example.demo.model.*;
import com.example.demo.repository.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class WycieczkaService {

    private final WycieczkaRepository wycieczkaRepository;

    private final OdznakaRepository odznakaRepository;

    private final TurystaRepository turystaRepository;

    private final TypOdznakiRepository typOdznakiRepository;

    private final TrasaWlasnaRepository trasaWlasnaRepository;

    public List<WycieczkaDto> findAllWycieczkiDoWeryfikacji(){
        return wycieczkaRepository.findByDoWeryfikacjiTrue().stream()
                .filter(w -> !w.getZweryfikowana())
                .map(WycieczkaDto::toWycieczkaDto)
                .collect(Collectors.toList());
    }

    public WycieczkaDto findWycieczka(Long wycieczkaId) throws WycieczkaNotFoundException {
        Optional<Wycieczka> wycieczkaOptional = wycieczkaRepository.findByWycieczkaId(wycieczkaId);
        if(wycieczkaOptional.isPresent())
            return WycieczkaDto.toWycieczkaDto(wycieczkaOptional.get());
        else
            throw new WycieczkaNotFoundException("Nie ma takiej wycieczki");
    }

    public void verifyWycieczka(Long wycieczkaId) throws WycieczkaNotFoundException, OdznakaExistsException, ZleDaneException {
        Optional<Wycieczka> wycieczkaOptional = wycieczkaRepository.findByWycieczkaId(wycieczkaId);
        if(wycieczkaOptional.isPresent() && !wycieczkaOptional.get().getZweryfikowana() && wycieczkaOptional.get().getDoWeryfikacji()) {
            Wycieczka wycieczka = wycieczkaOptional.get();
            if(wycieczka.getOdznaka() != null) {
                Optional<Odznaka> odznakaOptional = odznakaRepository.findOdznakaByTurystaAndTypOdznaki(wycieczka.getOdznaka().getTurysta(), wycieczka.getOdznaka().getTypOdznaki());
                if (odznakaOptional.isPresent()) {
                    Odznaka odznaka = odznakaOptional.get();
                    int punkty = wycieczka.getPunktyZaWycieczke();
                    addPoints(odznaka, punkty);
                    odznakaRepository.save(odznaka);
                    wycieczka.setDoWeryfikacji(false);
                    wycieczka.setZweryfikowana(true);
                    wycieczkaRepository.save(wycieczka);
                }
                else
                    throw new OdznakaExistsException("Nie ma takiej odznaki");
            }
            else
                throw new ZleDaneException("Zle podana odznaka");
        }
        else
            throw new WycieczkaNotFoundException("Nie ma takiej wycieczki lub została ona już zweryfikowana");
    }

    public void rejectWycieczka(Long wycieczkaId) throws WycieczkaNotFoundException{
        Optional<Wycieczka> wycieczkaOptional = wycieczkaRepository.findByWycieczkaId(wycieczkaId);
        if(wycieczkaOptional.isPresent()) {
            Wycieczka wycieczka = wycieczkaOptional.get();
            wycieczka.setDoWeryfikacji(false);
            wycieczkaRepository.save(wycieczka);
        }
        else
            throw new WycieczkaNotFoundException("Nie ma takiej wycieczki");
    }

    private void addPoints(Odznaka odznaka, int points){
        int actualPoints = odznaka.getAktualnePunkty();
        if(odznaka.getTypOdznaki().getNazwa().equals("W gory")) {
            if (points > 15)
                odznaka.setAktualnePunkty(actualPoints + 15);
            else
                odznaka.setAktualnePunkty(actualPoints + points);
        }
        else{
            if(points > 50)
                odznaka.setAktualnePunkty(actualPoints + 50);
            else
                odznaka.setAktualnePunkty(actualPoints + points);
        }
    }

    public void createWycieczka(WycieczkaCreateDto wycieczkaCreateDto) {
        int punkty = wycieczkaCreateDto.getTrasaList().stream()
                .map(Trasa::obliczPunkty)
                .reduce(0, Integer::sum);
        Odznaka odznaka = odznakaRepository.findById(new TypOdznakiTurystaKey(1L, 1L)).get();

        wycieczkaCreateDto.getTrasaList().stream()
                .filter(w -> w.getTrasaId() == null)
                .forEach(w -> trasaWlasnaRepository.save(((TrasaWlasna) w)));

        Wycieczka wycieczka = Wycieczka.builder()
                .doWeryfikacji(false)
                .zweryfikowana(false)
                .nazwa(wycieczkaCreateDto.getNazwa())
                .punktyZaWycieczke(punkty)
                .odznaka(odznaka)
                .dataWycieczki(wycieczkaCreateDto.getDate())
                .trasaList(wycieczkaCreateDto.getTrasaList())
                .build();
        wycieczkaRepository.save(wycieczka);
    }

    public List<WycieczkaDto> findWycieczkaByTypAndTurysta(Long typId, Long turId) {
        Optional<Turysta> turystaOptional = turystaRepository.findById(turId);
        Optional<TypOdznaki> typOdznakiOptional = typOdznakiRepository.findById(typId);
        Optional<Odznaka> odznakaOptional = odznakaRepository.findOdznakaByTurystaAndTypOdznaki(turystaOptional.get(), typOdznakiOptional.get());
        return wycieczkaRepository.findByOdznaka(odznakaOptional.get()).stream()
                .map(WycieczkaDto::toWycieczkaDto)
                .collect(Collectors.toList());
    }
}
