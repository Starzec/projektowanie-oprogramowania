package com.example.demo.controller;

import com.example.demo.DTO.GrupaGorskaDto;
import com.example.demo.service.GrupaGorskaService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class GrupaGorskaController {

    private final GrupaGorskaService grupaGorskaService;

    @GetMapping(path = "/mountain-groups" , produces = APPLICATION_JSON_VALUE)
    public List<GrupaGorskaDto> dajGrupyGorskie(){
        return grupaGorskaService.findAllGrupyGorskie();
    }
}
