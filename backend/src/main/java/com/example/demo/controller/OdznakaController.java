package com.example.demo.controller;

import com.example.demo.DTO.OdznakaDto;
import com.example.demo.DTO.TypOdznakiDto;
import com.example.demo.exception.OdznakaExistsException;
import com.example.demo.exception.ZleDaneException;
import com.example.demo.service.OdznakaService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
@Slf4j
public class OdznakaController {

    private final OdznakaService odznakaService;

    @GetMapping(path = "/badges", produces = APPLICATION_JSON_VALUE)
    public List<OdznakaDto> dajOdznakiZdobyteTurysty(@RequestParam Long turystaId) throws ZleDaneException { //Normalnie wzięta ze Spring Security Context przy obsluzeniu logowania
        return odznakaService.findAllObtainedBadges(turystaId);
    }

    @GetMapping(path = "/badge-types", produces = APPLICATION_JSON_VALUE)
    public List<TypOdznakiDto> dajWszystkieOdznaki(){
        return odznakaService.findAllTypOdznaki();
    }

    @PostMapping(path = "/badge-types")
    public void dodajTypOdznaki(@RequestBody TypOdznakiDto typOdznakiDto) throws OdznakaExistsException, ZleDaneException {
        odznakaService.addTypOdznaki(typOdznakiDto);
    }
}
