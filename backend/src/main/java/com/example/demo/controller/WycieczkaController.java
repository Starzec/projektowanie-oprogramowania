package com.example.demo.controller;

import com.example.demo.DTO.WycieczkaCreateDto;
import com.example.demo.DTO.WycieczkaDto;
import com.example.demo.exception.OdznakaExistsException;
import com.example.demo.exception.WycieczkaNotFoundException;
import com.example.demo.exception.ZleDaneException;
import com.example.demo.service.WycieczkaService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class WycieczkaController {

    private final WycieczkaService wycieczkaService;

    @GetMapping(path = "/trip-verify", produces = APPLICATION_JSON_VALUE)
    public List<WycieczkaDto> dajWszystkieWycieczkiDoWeryfikacji(){
        return wycieczkaService.findAllWycieczkiDoWeryfikacji();
    }

    @GetMapping(path = "/trip", produces = APPLICATION_JSON_VALUE)
    public WycieczkaDto dajWycieczka(@RequestParam Long wycieczkaId) throws WycieczkaNotFoundException {   //Tu powinien byc request param, ale tak w dokumentacji zrobilismy, wiec w implementacji trzymamy się tego, wystarczy przekazac ID wycieczki
        return wycieczkaService.findWycieczka(wycieczkaId);
    }

    @PutMapping("/tour/accept")
    public void acceptWycieczke(@RequestParam Long wycieczkaId) throws WycieczkaNotFoundException, OdznakaExistsException, ZleDaneException {
        wycieczkaService.verifyWycieczka(wycieczkaId);
    }

    @PutMapping("/tour/reject")
    public void rejectWycieczka(@RequestParam Long wycieczkaId) throws WycieczkaNotFoundException {
        wycieczkaService.rejectWycieczka(wycieczkaId);
    }

    @PostMapping(path = "/tour", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public void dodajWycieczke(@RequestBody WycieczkaCreateDto wycieczkaCreateDto){
        wycieczkaService.createWycieczka(wycieczkaCreateDto);
    }

    @GetMapping(path="/trip/badge")
    public List<WycieczkaDto> dajWycieczkiOdznaki(@RequestParam Long typId, @RequestParam Long turId){
        return wycieczkaService.findWycieczkaByTypAndTurysta(typId, turId);
    }
}
