package com.example.demo.controller;

import com.example.demo.DTO.PasmoDto;
import com.example.demo.service.PasmoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class PasmoController {

    private final PasmoService pasmoService;

    @GetMapping(path = "/mountain-ranges", produces = APPLICATION_JSON_VALUE)
    public List<PasmoDto> dajPasmaZGrupy(@RequestParam Long grupaId){
        return pasmoService.findPasmaWithGrupa(grupaId);
    }


}
