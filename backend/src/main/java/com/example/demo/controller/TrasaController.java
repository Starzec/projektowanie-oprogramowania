package com.example.demo.controller;

import com.example.demo.DTO.TrasaDto;
import com.example.demo.service.TrasaService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class TrasaController {

    private final TrasaService trasaService;

    @GetMapping(path = "/track/byMountainRangeId")
    public List<TrasaDto> dajAktywneTrasyWRegionie(@RequestParam Long pasmoId){
        return trasaService.findAllDefinedTracesInRegion(pasmoId);
    }
}
