package com.example.demo.repository;

import com.example.demo.model.StatusTrasy;
import org.springframework.data.repository.CrudRepository;

public interface StatusTrasyRepository extends CrudRepository<StatusTrasy, String> {
}
