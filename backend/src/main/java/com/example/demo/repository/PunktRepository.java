package com.example.demo.repository;

import com.example.demo.model.Punkt;
import org.springframework.data.repository.CrudRepository;

public interface PunktRepository extends CrudRepository<Punkt, Long> {
}
