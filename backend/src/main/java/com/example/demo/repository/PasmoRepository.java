package com.example.demo.repository;

import com.example.demo.model.Pasmo;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PasmoRepository extends CrudRepository<Pasmo, Long> {
    List<Pasmo> findByGrupaGorskaGrupaGorskaId(Long id);
}
