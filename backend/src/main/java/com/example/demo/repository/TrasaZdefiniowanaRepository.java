package com.example.demo.repository;

import com.example.demo.model.TrasaZdefiniowana;

public interface TrasaZdefiniowanaRepository extends TrasaRepository<TrasaZdefiniowana> {
}
