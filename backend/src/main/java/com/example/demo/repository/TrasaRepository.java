package com.example.demo.repository;

import com.example.demo.model.Trasa;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TrasaRepository<T extends Trasa> extends CrudRepository<T, Long> {
    List<T> findAllByPasmoPasmoIdAndStatusTrasyStatusTrasy(Long pasmoId, String statusTrasy);
}
