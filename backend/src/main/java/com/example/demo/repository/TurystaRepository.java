package com.example.demo.repository;

import com.example.demo.model.Turysta;
import org.springframework.data.repository.CrudRepository;

public interface TurystaRepository extends CrudRepository<Turysta, Long> {
}
