package com.example.demo.repository;

import com.example.demo.model.GrupaGorska;
import org.springframework.data.repository.CrudRepository;

public interface GrupaGorskaRepository extends CrudRepository<GrupaGorska, Long> {
}
