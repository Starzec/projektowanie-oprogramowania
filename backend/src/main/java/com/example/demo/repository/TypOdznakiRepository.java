package com.example.demo.repository;

import com.example.demo.model.TypOdznaki;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TypOdznakiRepository extends CrudRepository<TypOdznaki, Long> {
    List<TypOdznaki> findAll();
    boolean existsByNazwa(String nazwa);
    TypOdznaki save(TypOdznaki typOdznaki);
}
