package com.example.demo.repository;

import com.example.demo.model.Odznaka;
import com.example.demo.model.Turysta;
import com.example.demo.model.TypOdznaki;
import com.example.demo.model.TypOdznakiTurystaKey;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface OdznakaRepository extends CrudRepository<Odznaka, TypOdznakiTurystaKey> {
    Optional<Odznaka> findOdznakaByTurystaAndTypOdznaki(Turysta turysta, TypOdznaki typOdznaki);
    Odznaka save(Odznaka odznaka);
    List<Odznaka> findByTurysta(Turysta turysta);
    List<Odznaka> findAllByTurysta(Turysta turysta);
}
