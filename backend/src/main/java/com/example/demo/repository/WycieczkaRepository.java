package com.example.demo.repository;

import com.example.demo.model.Odznaka;
import com.example.demo.model.Wycieczka;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface WycieczkaRepository extends CrudRepository<Wycieczka, Long> {
    List<Wycieczka> findByDoWeryfikacjiTrue();
    Optional<Wycieczka> findByWycieczkaId(Long id);
    Wycieczka save(Wycieczka wycieczka);
    List<Wycieczka> findByOdznaka(Odznaka odznaka);
}
