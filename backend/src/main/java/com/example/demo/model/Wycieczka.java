package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Builder
public class Wycieczka {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "wycieczka_id")
    private Long wycieczkaId;

    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate dataWycieczki;

    private Integer punktyZaWycieczke;

    private String nazwa;

    private Boolean doWeryfikacji;

    private Boolean zweryfikowana;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name="typ_odznaki_id", referencedColumnName = "typ_odznaki_id"),
            @JoinColumn(name = "turysta_id", referencedColumnName = "turysta_id")
    })
    private Odznaka odznaka;

    @Builder.Default
    @ManyToMany
    @JoinTable(
            name = "wycieczka_trasa",
            joinColumns = @JoinColumn(name = "wycieczka_id"),
            inverseJoinColumns = @JoinColumn(name = "trasa_id")
    )
    private List<Trasa> trasaList = new ArrayList<>();
}
