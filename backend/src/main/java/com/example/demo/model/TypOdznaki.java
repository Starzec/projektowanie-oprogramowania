package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Builder
public class TypOdznaki {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "typ_odznaki_id")
    private Long typOdznakiId;

    private Integer wymaganePunkty;

    private String nazwa;

    private String zdjecie;
}
