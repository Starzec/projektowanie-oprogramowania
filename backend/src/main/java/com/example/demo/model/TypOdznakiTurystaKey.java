package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TypOdznakiTurystaKey implements Serializable {

    @Column(name = "typ_odznaki_id")
    Long typOdznakiId;

    @Column(name = "turysta_id")
    Long turystaId;
}
