package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@PrimaryKeyJoinColumn(name = "turysta_id")
public class Turysta extends Uzytkownik{

    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate dataUrodzenia;

    public Turysta(Long userId, String imie, String nazwisko, String mail, String haslo, String zdjecie, LocalDate dataUrodzenia) {
        super(userId, imie, nazwisko, mail, haslo, zdjecie);
        this.dataUrodzenia = dataUrodzenia;
    }
}
