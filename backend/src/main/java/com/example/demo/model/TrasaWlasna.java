package com.example.demo.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@PrimaryKeyJoinColumn(name = "trasa_wlasna_id")
public class TrasaWlasna extends Trasa{

    private Integer roznicaWysokosci;

    private Integer dlugosc;

    private Integer punktyZaPrzejscie;

    public TrasaWlasna(Long trasaId, LocalDate dataUtworzenia, String nazwa, Pasmo pasmo, StatusTrasy statusTrasy, Integer roznicaWysokosci, Integer dlugosc, Integer punktyZaPrzejscie) {
        super(trasaId, dataUtworzenia, nazwa, pasmo, statusTrasy);
        this.roznicaWysokosci = roznicaWysokosci;
        this.dlugosc = dlugosc;
        this.punktyZaPrzejscie = punktyZaPrzejscie;
    }

    @Override
    public int obliczPunkty() {
        return punktyZaPrzejscie;
    }
}
