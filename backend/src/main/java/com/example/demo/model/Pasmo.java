package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Pasmo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pasmo_id")
    private Long pasmoId;

    private String nazwa;

    @ManyToOne
    @JoinColumn(name = "grupa_gorska_id")
    private GrupaGorska grupaGorska;
}
