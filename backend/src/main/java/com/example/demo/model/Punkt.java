package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Punkt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "punkt_id")
    private Long punktId;

    private String nazwa;

    private Integer wysokoscNadPoziomemMorza;

    private Integer szerokoscGeograficzna;

    private Integer dlugoscGeograficzna;
}
