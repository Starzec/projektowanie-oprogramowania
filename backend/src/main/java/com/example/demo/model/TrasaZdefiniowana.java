package com.example.demo.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@PrimaryKeyJoinColumn(name = "trasa_zdefiniowana_id")
public class TrasaZdefiniowana extends Trasa{

    private Integer punkty;

    @ManyToOne
    @JoinColumn(name = "punkt_poczatkowy_id")
    private Punkt punktPoczatkowy;

    @ManyToOne
    @JoinColumn(name = "punkt_koncowy_id")
    private Punkt punktKoncowy;

    public TrasaZdefiniowana(Long trasaId, LocalDate dataUtworzenia, String nazwa, Pasmo pasmo, StatusTrasy statusTrasy, Integer punkty, Punkt punktPoczatkowy, Punkt punktKoncowy) {
        super(trasaId, dataUtworzenia, nazwa, pasmo, statusTrasy);
        this.punkty = punkty;
        this.punktPoczatkowy = punktPoczatkowy;
        this.punktKoncowy = punktKoncowy;
    }

    @Override
    public int obliczPunkty() {
        return punkty;
    }
}
