package com.example.demo.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Odznaka {

    @EmbeddedId
    private TypOdznakiTurystaKey id;

    @ManyToOne
    @MapsId("typ_odznaki_id")
    @JoinColumn(name = "typ_odznaki_id")
    private TypOdznaki typOdznaki;

    @ManyToOne
    @MapsId("turysta_id")
    @JoinColumn(name = "turysta_id")
    private Turysta turysta;

    private Boolean czyZdobyta;

    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate rokZdobycia;

    private Integer aktualnePunkty;
}
