package com.example.demo.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value=TrasaWlasna.class, name = "TrasaWlasna"),
        @JsonSubTypes.Type(value = TrasaZdefiniowana.class, name = "TrasaZdefiniowana")
})
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Trasa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "trasa_id")
    private Long trasaId;

    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate dataUtworzenia;

    private String nazwa;

    @ManyToOne
    @JoinColumn(name = "pasmo_id")
    private Pasmo pasmo;

    @ManyToOne
    @JoinColumn(name = "status_trasy")
    private StatusTrasy statusTrasy;

    public abstract int obliczPunkty();
}
