CREATE TABLE grupa_gorska(
  grupa_gorska_id BIGSERIAL PRIMARY KEY,
  nazwa VARCHAR(255)
);

CREATE TABLE pasmo(
    pasmo_id BIGSERIAL PRIMARY KEY,
    nazwa VARCHAR(255),
    grupa_gorska_id BIGINT REFERENCES grupa_gorska (grupa_gorska_id) NOT NULL
);

CREATE TABLE uzytkownik(
    user_id BIGSERIAL PRIMARY KEY,
    imie VARCHAR(255),
    nazwisko VARCHAR(255),
    mail VARCHAR(255),
    haslo VARCHAR(255),
    zdjecie VARCHAR(255)
);

CREATE TABLE turysta(
    turysta_id BIGSERIAL PRIMARY KEY,
    data_urodzenia DATE
);

CREATE TABLE typ_odznaki(
    typ_odznaki_id BIGSERIAL PRIMARY KEY,
    wymagane_punkty INTEGER,
    nazwa VARCHAR(255),
    zdjecie TEXT
);

CREATE TABLE odznaka(
    typ_odznaki_id BIGINT REFERENCES typ_odznaki(typ_odznaki_id) NOT NULL,
    turysta_id BIGINT REFERENCES Turysta(turysta_id) NOT NULL,
    czy_zdobyta BOOLEAN DEFAULT FALSE,
    rok_zdobycia date,
    aktualne_punkty INTEGER,
    PRIMARY KEY(typ_odznaki_id, turysta_id)
);

CREATE TABLE status_trasy(
  status_trasy VARCHAR(255) PRIMARY KEY
);

CREATE TABLE wycieczka(
    wycieczka_id BIGSERIAL PRIMARY KEY,
    data_wycieczki date,
    punkty_za_wycieczke INTEGER,
    nazwa VARCHAR(255),
    do_weryfikacji BOOLEAN,
    zweryfikowana BOOLEAN,
    typ_odznaki_id BIGINT NOT NULL,
    turysta_id BIGINT NOT NULL,
    FOREIGN KEY (turysta_id, typ_odznaki_id) REFERENCES Odznaka(turysta_id, typ_odznaki_id)
);

CREATE TABLE trasa(
    trasa_id BIGSERIAL PRIMARY KEY,
    data_utworzenia DATE,
    nazwa VARCHAR(255),
    pasmo_id BIGINT REFERENCES Pasmo(pasmo_id) NOT NULL,
    status_trasy VARCHAR(255) REFERENCES status_trasy(status_trasy) NOT NULL
);

CREATE TABLE wycieczka_trasa(
  trasa_id BIGINT REFERENCES Trasa(trasa_id) NOT NULL,
  wycieczka_id BIGINT REFERENCES Wycieczka(wycieczka_id) NOT NULL,
  PRIMARY KEY (trasa_id, wycieczka_id)
);

CREATE TABLE trasa_wlasna(
    trasa_wlasna_id BIGSERIAL PRIMARY KEY,
    roznica_wysokosci INTEGER,
    dlugosc INTEGER,
    punkty_za_przejscie INTEGER,
    FOREIGN KEY (trasa_wlasna_id) REFERENCES trasa(trasa_id)
);

CREATE TABLE punkt(
    punkt_id BIGSERIAL PRIMARY KEY,
    nazwa VARCHAR(255),
    wysokosc_nad_poziomem_morza INTEGER,
    szerokosc_geograficzna INTEGER,
    dlugosc_geograficzna INTEGER
);

CREATE TABLE trasa_zdefiniowana(
    trasa_zdefiniowana_id BIGSERIAL PRIMARY KEY,
    punkty INTEGER,
    punkt_poczatkowy_id BIGINT REFERENCES punkt(punkt_id) NOT NULL,
    punkt_koncowy_id BIGINT REFERENCES punkt(punkt_id) NOT NULL,
    FOREIGN KEY (trasa_zdefiniowana_id) REFERENCES trasa(trasa_id)
);
