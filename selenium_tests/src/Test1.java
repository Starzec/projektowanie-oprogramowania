import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

class Test1 {

    private static ChromeDriver driver;

    @BeforeAll
    static void initialize() {
        String SYSTEM_PROPERTY_1 = "webdriver.chrome.driver";
        String SYSTEM_PROPERTY_2 = "C:\\Users\\zaraki\\Documents\\chromedriver.exe";

        System.setProperty(SYSTEM_PROPERTY_1, SYSTEM_PROPERTY_2);
        driver = new ChromeDriver();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
    } // initialize


    @Test
    void test_dodania_typu_odznaki() throws InterruptedException {

        driver.get("http://localhost:4200/dodajTypOdznaki");

        sleep(1000L);
        WebElement nazwaOdznaki = driver.findElement(By.id("name"));
        nazwaOdznaki.sendKeys("automatyczna_nazwa345");

        sleep(1000L);
        WebElement liczbaPunktow = driver.findElement(By.id("requiredPoints"));
        liczbaPunktow.sendKeys("222");

        sleep(1000L);
        WebElement zdjecie = driver.findElement(By.id("picture"));
        zdjecie.sendKeys("C:\\Users\\zaraki\\Desktop\\PO_apka\\projektowanie-oprogramowania\\selenium_tests\\res\\132.jpg");

        sleep(1000L);
        WebElement submitButton = driver.findElement(By.id("submitButton"));
        submitButton.click();
    }


    @Test
    void test_zaplanowania_wycieczki() throws InterruptedException {

        driver.get("http://localhost:4200/zaplanujWycieczke");

        sleep(1000L);
        WebElement grupaGorska = driver.findElement(By.xpath("//app-mountain-grup[1]//a[1]"));
        grupaGorska.click();

        sleep(1000L);
        WebElement pasmoGorskie = driver.findElement(By.xpath("//app-moutain-range[1]//a[1]"));
        pasmoGorskie.click();

        sleep(1000L);
        WebElement trasa1 = driver.findElement(By.xpath("//app-track-for-plan-trip[1]//a[1]"));
        trasa1.click();

        sleep(1000L);
        WebElement trasa2 = driver.findElement(By.xpath("//app-track-for-plan-trip[2]//a[1]"));
        trasa2.click();

//        dodawanie trasy własnej
        sleep(1000L);
        WebElement name = driver.findElement(By.id("name"));
        name.sendKeys("Trasa wlasna 1");

        sleep(1000L);
        WebElement distance = driver.findElement(By.id("distance"));
        distance.sendKeys("1111");

        sleep(1000L);
        WebElement heightDifference = driver.findElement(By.id("heightDifference"));
        heightDifference.sendKeys("520");

        sleep(1000L);
        WebElement addTrackButton = driver.findElement(By.xpath("//button[contains(text(),'Dodaj')]"));
        addTrackButton.click();

        sleep(1000L);
        WebElement submitButton = driver.findElement(By.xpath("//button[contains(text(),'Dalej')]"));
        submitButton.click();

        //potwierdzenie wycieczki
        WebElement tripName = driver.findElement(By.id("name"));
        tripName.sendKeys("automatyczna_wycieczka");

        WebElement dataWycieczki = driver.findElement(By.id("date"));
        dataWycieczki.sendKeys("20-04-2021");

        sleep(1000L);
        WebElement submitTripButton = driver.findElement(By.xpath("//button[@class='btn btn-success']"));
        submitTripButton.click();
    }

}













