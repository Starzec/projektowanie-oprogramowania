import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NavigationComponent} from './components/navigation/navigation.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {TripVerificationComponent} from './components/trip-verification/trip-verification.component';
import {TripComponent} from './components/trip-verification/trip/trip.component';
import {NotSelectedComponent} from './components/trip-verification/not-selected/not-selected.component';
import {TripDetailsComponent} from './components/trip-verification/trip-details/trip-details.component';
import {TrackComponent} from './components/trip-verification/trip-details/track/track.component';
import {ShowBadgesComponent} from './components/show-badges/show-badges.component';
import {BadgeListComponent} from './components/show-badges/badge-list/badge-list.component';
import {BadgeComponent} from './components/show-badges/badge-list/badge/badge.component';
import {BadgeDetailsComponent} from './components/show-badges/badge-list/badge/badge-details/badge-details.component';
import {TripForBadgesComponent} from './components/show-badges/badge-list/badge/badge-details/trip-for-badges/trip-for-badges.component';
import {TrackForBadgesComponent} from './components/show-badges/badge-list/badge/badge-details/trip-for-badges/track-for-badges/track-for-badges.component';
import {PlanTripComponent} from './components/plan-trip/plan-trip.component';
import {SelectMountainGroupComponent} from './components/plan-trip/select-mountain-grup/select-mountain-group.component';
import {SelectMountainRangeComponent} from './components/plan-trip/select-mountain-grup/select-mountain-range/select-mountain-range.component';
import {AddTracksComponent} from './components/plan-trip/add-tracks/add-tracks.component';
import {ConfirmTripComponent} from './components/plan-trip/add-tracks/confirm-trip/confirm-trip.component';
import {MountainGrupComponent} from './components/plan-trip/select-mountain-grup/mountain-grup/mountain-grup.component';
import {MoutainRangeComponent} from './components/plan-trip/select-mountain-grup/select-mountain-range/moutain-range/moutain-range.component';
import { TrackForPlanTripComponent } from './components/plan-trip/add-tracks/track-for-plan-trip/track-for-plan-trip.component';
import { AddOwnTrackComponent } from './components/plan-trip/add-tracks/add-own-track/add-own-track.component';
import { AddBadgeTypeComponent } from './components/add-badge-type/add-badge-type.component';
import { BadgesListComponent } from './components/add-badge-type/badges-list/badges-list.component';
import { NewBadgeFormComponent } from './components/add-badge-type/new-badge-form/new-badge-form.component';
import { BadgeTypeComponent } from './components/add-badge-type/badges-list/badge-type/badge-type.component';
import { AcceptedComponent } from './components/trip-verification/accepted/accepted.component';
import {RejectedComponent} from './components/trip-verification/rejected/rejected.component';
import { NotSelectedBadgeComponent } from './components/show-badges/not-selected-badge/not-selected-badge.component';

const routes: Routes = [
  {path: '', redirectTo: '/dodajTypOdznaki', pathMatch: 'full'},
  {
    path: 'doWeryfikacji',
    component: TripVerificationComponent,
    children: [
      {path: '', component: NotSelectedComponent},
      {path: 'zaakceptowana', component: AcceptedComponent},
      {path: 'odrzucona', component: RejectedComponent},
      {path: ':id', component: TripDetailsComponent},
    ]
  },
  {
    path: 'pokazOdznaki',
    component: ShowBadgesComponent,
    children: [
      {path: '', component: NotSelectedBadgeComponent},
      {
        path: ':turistId/:typeId', component: BadgeDetailsComponent,
        children: [
          {path: ':tripId', component: TrackForBadgesComponent},
        ]
      },
    ]
  },
  {
    path: 'zaplanujWycieczke',
    component: PlanTripComponent,
    children: [
      {
        path: '',
        component: SelectMountainGroupComponent,
        children: [
          {
            path: ':id',
            component: SelectMountainRangeComponent,
          },
        ]
      },]
  },
  {path: 'dodajWycieczke/:groupId/:rangeId', component: AddTracksComponent},
  {path: 'potwierdzNowaWycieczke', component: ConfirmTripComponent},
  {path: 'dodajTypOdznaki', component: AddBadgeTypeComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    TripVerificationComponent,
    TripComponent,
    NotSelectedComponent,
    TripDetailsComponent,
    TrackComponent,
    ShowBadgesComponent,
    BadgeListComponent,
    BadgeComponent,
    BadgeDetailsComponent,
    TripForBadgesComponent,
    TrackForBadgesComponent,
    PlanTripComponent,
    SelectMountainGroupComponent,
    SelectMountainRangeComponent,
    AddTracksComponent,
    ConfirmTripComponent,
    MountainGrupComponent,
    MoutainRangeComponent,
    TrackForPlanTripComponent,
    AddOwnTrackComponent,
    AddBadgeTypeComponent,
    BadgesListComponent,
    NewBadgeFormComponent,
    BadgeTypeComponent,
    AcceptedComponent,
    NotSelectedBadgeComponent,
    RejectedComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
