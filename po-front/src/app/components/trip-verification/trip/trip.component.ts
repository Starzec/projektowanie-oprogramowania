import {Component, Input, OnInit} from '@angular/core';
import {Wycieczka} from '../../../models/Wycieczka';

@Component({
  selector: 'app-trip',
  templateUrl: './trip.component.html',
  styleUrls: ['./trip.component.css']
})
export class TripComponent implements OnInit {

  @Input() trip: Wycieczka;

  constructor() { }

  ngOnInit() {
  }

}
