import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Wycieczka} from '../../../models/Wycieczka';
import {TripService} from '../../../models/services/TripService';
import {Trasa} from '../../../models/Trasa';
import {TrackService} from '../../../models/services/TrackService';
import {TrackMapper} from "../../../models/services/TrackMapper";

@Component({
  selector: 'app-trip-details',
  templateUrl: './trip-details.component.html',
  styleUrls: ['./trip-details.component.css']
})
export class TripDetailsComponent implements OnInit {

  private tripId: number;
  private trip: Wycieczka;
  private tracks: Trasa[];

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private tripService: TripService,
              private trackService: TrackService,
              private trackMapper: TrackMapper) {
  }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe(
        (params: Params) => {
          this.tripId = +params['id'];
          this.tripService.getTripById(this.tripId)
            .subscribe((trip: Wycieczka) => {
              this.trip = trip;
              this.tracks = this.trackMapper.listFromJson(this.trip.trasaList);
            });
        }
      );
  }

  getPointsSum(): number {
    var sum: number = 0;
    for (let track of this.tracks) {
      sum = sum + track.getPunkty();
    }
    return sum;
  }

  onReject() {
    this.tripService.rejectTrip(this.tripId);
    this.router.navigate(["doWeryfikacji/odrzucona"]);
  }

  onAccept() {
    this.tripService.verifyTrip(this.tripId);
    this.router.navigate(["doWeryfikacji/zaakceptowana"]);
  }
}
