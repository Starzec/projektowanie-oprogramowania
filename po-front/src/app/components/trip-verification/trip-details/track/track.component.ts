import {Component, Input, OnInit} from '@angular/core';
import {Trasa} from '../../../../models/Trasa';
import {TrackService} from '../../../../models/services/TrackService';
import {ActivatedRoute, Params} from '@angular/router';
import {TrasaWlasna} from "../../../../models/TrasaWlasna";

@Component({
  selector: 'app-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.css']
})
export class TrackComponent implements OnInit {

  @Input() track: Trasa;
  private description: string;
  private points: number;

  constructor() { }

  ngOnInit() {
    this.points = this.track.getPunkty();
    // this.points = this.track.punktyZaPrzejscie;
    // this.description = this.track.nazwa;
    this.description = this.track.getDescription();
  }

}
