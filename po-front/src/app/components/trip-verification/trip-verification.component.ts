import {Component, OnInit} from '@angular/core';
import {TripService} from '../../models/services/TripService';
import {Wycieczka} from '../../models/Wycieczka';
import {Trasa} from '../../models/Trasa';

@Component({
  selector: 'app-trip-verification',
  templateUrl: './trip-verification.component.html',
  styleUrls: ['./trip-verification.component.css']
})
export class TripVerificationComponent implements OnInit {

  private tripsForVerification: Wycieczka[];

  constructor(private tripService: TripService) {
  }

  ngOnInit() {
    this.tripService.getTripsForVerification()
      .subscribe((trips: Wycieczka[]) => {
        this.tripsForVerification = trips;
      });
    this.tripService.verifiedTripsSubject
      .subscribe(
        () => {
          this.tripService.getTripsForVerification()
            .subscribe((trips: Wycieczka[]) => {
              this.tripsForVerification = trips;
            });
        }
      )
    const butt = document.getElementById('weryfikuj');
    butt.classList.add('active');
  }
}
