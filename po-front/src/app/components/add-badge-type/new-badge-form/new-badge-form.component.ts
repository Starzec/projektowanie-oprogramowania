import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BadgeTypeService} from '../../../models/services/BadgeTypeService';
import {TypOdznaki} from '../../../models/TypOdznaki';
import {CreateBadgeTypeDto} from '../../../models/shared/CreateBadgeTypeDto';

@Component({
  selector: 'app-new-badge-form',
  templateUrl: './new-badge-form.component.html',
  styleUrls: ['./new-badge-form.component.css']
})
export class NewBadgeFormComponent implements OnInit {

  private badgeTypeForm: FormGroup;
  private addingBadgeFailed: boolean = false;
  image: string;

  constructor(private badgeTypeServis: BadgeTypeService) {
  }

  ngOnInit() {
    this.badgeTypeForm = new FormGroup({
      name: new FormControl('', Validators.required),
      requiredPoints: new FormControl('', Validators.required),
      image: new FormControl(null, Validators.required),
    });
  }

  onSubmit() {
    let values = this.badgeTypeForm.value;
    let newBadge = new CreateBadgeTypeDto(values.requiredPoints, values.name, this.image);
    this.badgeTypeServis.createNewBadgeType(newBadge).subscribe(
      () => {
        this.badgeTypeServis.getAllBadgeTypes();
        this.badgeTypeForm.reset();
        this.addingBadgeFailed = false;
      },
      error => {
        if (error.status == 409) {
          this.addingBadgeFailed = true;
        }
      }
    );
  }

  onFileSelected(event) {
    if (event.target.files && event.target.files.length > 0) {
      let currentComponent = this;
      let file = event.target.files[0];
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function() {
        let result = reader.result;
        if (typeof result === 'string') {
          currentComponent.image = <string> reader.result;
        } else {
          currentComponent.image = String.fromCharCode.apply(null, new Uint16Array(<ArrayBuffer> reader.result));
        }
      };
    }
  }

}
