import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewBadgeFormComponent } from './new-badge-form.component';

describe('NewBadgeFormComponent', () => {
  let component: NewBadgeFormComponent;
  let fixture: ComponentFixture<NewBadgeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewBadgeFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBadgeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
