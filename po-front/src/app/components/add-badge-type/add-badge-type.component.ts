import { Component, OnInit } from '@angular/core';
import {TrackService} from '../../models/services/TrackService';

@Component({
  selector: 'app-add-badge-type',
  templateUrl: './add-badge-type.component.html',
  styleUrls: ['./add-badge-type.component.css']
})
export class AddBadgeTypeComponent implements OnInit {

  constructor(private track: TrackService) { }

  ngOnInit() {
    const butt = document.getElementById('odznaki');
    butt.classList.add('active');
    this.track.calculatePoints(5500, 151);
  }

}
