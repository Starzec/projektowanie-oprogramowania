import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBadgeTypeComponent } from './add-badge-type.component';

describe('AddBadgeTypeComponent', () => {
  let component: AddBadgeTypeComponent;
  let fixture: ComponentFixture<AddBadgeTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBadgeTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBadgeTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
