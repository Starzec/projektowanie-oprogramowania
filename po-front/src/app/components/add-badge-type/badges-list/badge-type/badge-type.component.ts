import {Component, Input, OnInit} from '@angular/core';
import {TypOdznaki} from '../../../../models/TypOdznaki';

@Component({
  selector: 'app-badge-type',
  templateUrl: './badge-type.component.html',
  styleUrls: ['./badge-type.component.css']
})
export class BadgeTypeComponent implements OnInit {

  @Input() badgeType: TypOdznaki;

  constructor() { }

  ngOnInit() {
  }

}
