import { Component, OnInit } from '@angular/core';
import {TypOdznaki} from '../../../models/TypOdznaki';
import {BadgeTypeService} from '../../../models/services/BadgeTypeService';

@Component({
  selector: 'app-badges-list',
  templateUrl: './badges-list.component.html',
  styleUrls: ['./badges-list.component.css']
})
export class BadgesListComponent implements OnInit {

  private badgeTypesList: TypOdznaki[];

  constructor(private badgeTypeService: BadgeTypeService) { }

  ngOnInit() {
    this.badgeTypeService.getAllBadgeTypes();
    this.badgeTypeService.typesListSubject.subscribe((badgeTypes: TypOdznaki[]) => {
      this.badgeTypesList = badgeTypes;
    });
  }

}
