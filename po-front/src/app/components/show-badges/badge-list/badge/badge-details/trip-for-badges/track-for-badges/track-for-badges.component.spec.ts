import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackForBadgesComponent } from './track-for-badges.component';

describe('TrackForBadgesComponent', () => {
  let component: TrackForBadgesComponent;
  let fixture: ComponentFixture<TrackForBadgesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackForBadgesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackForBadgesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
