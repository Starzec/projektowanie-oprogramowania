import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TripForBadgesComponent } from './trip-for-badges.component';

describe('TripForBadgesComponent', () => {
  let component: TripForBadgesComponent;
  let fixture: ComponentFixture<TripForBadgesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TripForBadgesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TripForBadgesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
