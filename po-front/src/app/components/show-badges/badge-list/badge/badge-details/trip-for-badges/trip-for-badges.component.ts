import {Component, Input, OnInit} from '@angular/core';
import {Wycieczka} from '../../../../../../models/Wycieczka';

@Component({
  selector: 'app-trip-for-badges',
  templateUrl: './trip-for-badges.component.html',
  styleUrls: ['./trip-for-badges.component.css']
})
export class TripForBadgesComponent implements OnInit {

  @Input() trip: Wycieczka;

  constructor() { }

  ngOnInit() {
  }

}
