import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {Wycieczka} from '../../../../../models/Wycieczka';
import {TripService} from '../../../../../models/services/TripService';
import {Trasa} from '../../../../../models/Trasa';
import {TrackService} from '../../../../../models/services/TrackService';

@Component({
  selector: 'app-badge-details',
  templateUrl: './badge-details.component.html',
  styleUrls: ['./badge-details.component.css']
})
export class BadgeDetailsComponent implements OnInit {

  private turistId: number;
  private badgeTyoeId: number;
  private trips: Wycieczka[];
  private tracks: Trasa[];

  constructor(private activatedRoute: ActivatedRoute, private tripService: TripService,
              private trackService: TrackService) {
  }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe(
        (params: Params) => {
          this.turistId = +params['turistId'];
          this.badgeTyoeId = +params['typeId'];
          this.tripService.getTripsByTouristIdAndBadgeTypeId(this.turistId, this.badgeTyoeId)
            .subscribe((trips: Wycieczka[]) => {
              this.trips = trips;
            });
        }
      );
  }

}
