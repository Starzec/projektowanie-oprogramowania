import {Component, OnInit} from '@angular/core';
import {TrackService} from '../../../../../../../models/services/TrackService';
import {Trasa} from '../../../../../../../models/Trasa';
import {ActivatedRoute, Params} from '@angular/router';
import {TripService} from "../../../../../../../models/services/TripService";
import {Wycieczka} from "../../../../../../../models/Wycieczka";
import {TrackMapper} from "../../../../../../../models/services/TrackMapper";

@Component({
  selector: 'app-track-for-badges',
  templateUrl: './track-for-badges.component.html',
  styleUrls: ['./track-for-badges.component.css']
})
export class TrackForBadgesComponent implements OnInit {

  private tracks: Trasa[];
  private tripId: number;

  constructor(private  trackService: TrackService, private activatedRoute: ActivatedRoute,
              private tripService: TripService, private trackMapper: TrackMapper) {
  }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe(
        (params: Params) => {
          this.tripId = +params['tripId'];
          this.tripService.getTripById(this.tripId)
            .subscribe((trip: Wycieczka) => {
              this.tracks = this.trackMapper.listFromJson(trip.trasaList);
            })
        }
      );
  }

}
