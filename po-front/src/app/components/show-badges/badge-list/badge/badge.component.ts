import {Component, Input, OnInit} from '@angular/core';
import {Odznaka} from '../../../../models/Odznaka';

@Component({
  selector: 'app-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.css']
})
export class BadgeComponent implements OnInit {

  @Input() badge: Odznaka;

  constructor() { }

  ngOnInit() {
  }

}
