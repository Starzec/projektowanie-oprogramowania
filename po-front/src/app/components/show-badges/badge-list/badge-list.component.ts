import {Component, OnInit} from '@angular/core';
import {BadgeService} from '../../../models/services/BadgeService';
import {Odznaka} from '../../../models/Odznaka';

@Component({
  selector: 'app-badge-list',
  templateUrl: './badge-list.component.html',
  styleUrls: ['./badge-list.component.css']
})
export class BadgeListComponent implements OnInit {

  private badges: Odznaka[];

  constructor(private badgeService: BadgeService) {
  }

  ngOnInit() {
    this.badgeService.turistBadgesSubject
      .subscribe((badges: Odznaka[]) => {
      this.badges = badges;
    });
    this.badgeService.getBadgesListForTurist(1);
  }

}
