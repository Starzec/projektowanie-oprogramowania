import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotSelectedBadgeComponent } from './not-selected-badge.component';

describe('NotSelectedBadgeComponent', () => {
  let component: NotSelectedBadgeComponent;
  let fixture: ComponentFixture<NotSelectedBadgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotSelectedBadgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotSelectedBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
