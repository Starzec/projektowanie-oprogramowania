import {Component, OnInit} from '@angular/core';
import {BadgeService} from '../../models/services/BadgeService';
import {Odznaka} from '../../models/Odznaka';
import {GrupaGorska} from "../../models/GrupaGorska";

@Component({
  selector: 'app-show-badges',
  templateUrl: './show-badges.component.html',
  styleUrls: ['./show-badges.component.css']
})
export class ShowBadgesComponent implements OnInit {

  private currentBadge: Odznaka;

  constructor(private badgeService: BadgeService) {
  }

  ngOnInit() {
    this.badgeService.currentBadgeSubject
      .subscribe((badge: Odznaka) => {
        this.currentBadge = badge;
      });
    this.badgeService.getBadgesListForTurist(1);
    const butt = document.getElementById('odznaki');
    butt.classList.add('active');
  }

}
