import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {TrasaWlasna} from '../../../../models/TrasaWlasna';
import {ActivatedRoute, Params} from '@angular/router';
import {MountainRangeService} from '../../../../models/services/MountainRangeService';
import {Pasmo} from '../../../../models/Pasmo';
import {StatusTrasy} from '../../../../models/StatusTrasy';
import {TrackService} from '../../../../models/services/TrackService';
import {AddTripService} from '../../../../models/services/AddTripService';

@Component({
  selector: 'app-add-own-track',
  templateUrl: './add-own-track.component.html',
  styleUrls: ['./add-own-track.component.css']
})
export class AddOwnTrackComponent implements OnInit {

  private ownTrackForm: FormGroup;
  private mountainRangeId: number;
  private mountainRange: Pasmo;

  constructor(private activatedRoute:ActivatedRoute,
              private mountainRangeService: MountainRangeService,
              private trackService: TrackService,
              private addTripService: AddTripService) { }

  ngOnInit() {
    this.ownTrackForm = new FormGroup({
      name: new FormControl('', Validators.required),
      distance: new FormControl('', Validators.required),
      heightDifference: new FormControl('', Validators.required),
    });
    this.activatedRoute.params
      .subscribe(
        (params: Params) => {
          this.mountainRangeId = +params['rangeId'];
          this.mountainRange = this.mountainRangeService.getMountainRangeById(this.mountainRangeId);
        }
      );
  }

  onSubmit() {
    let values = this.ownTrackForm.value;
    let status =  new StatusTrasy('aktywna');
    let data =  null;
    let points = this.trackService.calculatePoints(values.distance, values.heightDifference);
    let newTrack = new TrasaWlasna(null, this.mountainRange, status,
      data, values.name, values.heightDifference, values.distance,
      points, 'TrasaWlasna');
    this.addTripService.addToNewList(newTrack);
    this.ownTrackForm.reset();
  }

}
