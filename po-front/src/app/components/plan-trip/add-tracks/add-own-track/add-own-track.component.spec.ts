import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOwnTrackComponent } from './add-own-track.component';

describe('AddOwnTrackComponent', () => {
  let component: AddOwnTrackComponent;
  let fixture: ComponentFixture<AddOwnTrackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOwnTrackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOwnTrackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
