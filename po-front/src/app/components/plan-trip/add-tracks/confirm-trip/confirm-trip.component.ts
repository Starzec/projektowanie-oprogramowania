import { Component, OnInit } from '@angular/core';
import {AddTripService} from '../../../../models/services/AddTripService';
import { Trasa } from 'src/app/models/Trasa';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-confirm-trip',
  templateUrl: './confirm-trip.component.html',
  styleUrls: ['./confirm-trip.component.css']
})
export class ConfirmTripComponent implements OnInit {

  private selectedTracksList: Trasa[];
  private tripForm: FormGroup;
  private pointsSum: number;
  private currentDate: Date;

  constructor(private addTripService: AddTripService, private router: Router) { }

  ngOnInit() {
    this.currentDate = new Date();
    this.selectedTracksList = this.addTripService.getNewTrackList();
    this.tripForm = new FormGroup({
      name: new FormControl('', Validators.required),
      date: new FormControl('', Validators.required),
    });
    this.pointsSum = this.addTripService.calculatePoints();
  }

  onSubmit() {
    this.addTripService.saveTrip(this.tripForm.value.name, this.tripForm.value.date);
    this.tripForm.reset();
    this.router.navigate(['/zaplanujWycieczke']);
  }

}




























