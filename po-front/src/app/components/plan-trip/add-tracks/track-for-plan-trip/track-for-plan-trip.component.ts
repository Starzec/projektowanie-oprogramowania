import {Component, Input, OnInit} from '@angular/core';
import {Trasa} from '../../../../models/Trasa';
import {AddTripService} from '../../../../models/services/AddTripService';

@Component({
  selector: 'app-track-for-plan-trip',
  templateUrl: './track-for-plan-trip.component.html',
  styleUrls: ['./track-for-plan-trip.component.css']
})
export class TrackForPlanTripComponent implements OnInit {
  @Input() track: Trasa;
  private description: string;
  private points: number;

  constructor(private addTripService: AddTripService) { }

  ngOnInit() {
    this.points = this.track.getPunkty();
    this.description = this.track.getDescription();
  }

  onClick() {
    this.addTripService.moveToAnotherList(this.track);
  }

}
