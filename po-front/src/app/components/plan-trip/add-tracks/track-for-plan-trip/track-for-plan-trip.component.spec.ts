import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackForPlanTripComponent } from './track-for-plan-trip.component';

describe('TrackForPlanTripComponent', () => {
  let component: TrackForPlanTripComponent;
  let fixture: ComponentFixture<TrackForPlanTripComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackForPlanTripComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackForPlanTripComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
