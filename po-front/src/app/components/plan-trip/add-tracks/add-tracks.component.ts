import {Component, OnDestroy, OnInit} from '@angular/core';
import {AddTripService} from '../../../models/services/AddTripService';
import {TrackService} from '../../../models/services/TrackService';
import {Trasa} from '../../../models/Trasa';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {TrackMapper} from "../../../models/services/TrackMapper";
import {Pasmo} from '../../../models/Pasmo';
import {GrupaGorska} from '../../../models/GrupaGorska';
import {MountainRangeService} from '../../../models/services/MountainRangeService';

@Component({
  selector: 'app-add-tracks',
  templateUrl: './add-tracks.component.html',
  styleUrls: ['./add-tracks.component.css'],
})
export class AddTracksComponent implements OnInit, OnDestroy {

  private allTracks: Trasa[];
  private selectedTracks: Trasa[] = [];
  private mountainRangeId: number;
  private mountainGroupId: number;
  private mountainRangeName: string;
  private mountainGroupName: string;
  private subscription1: Subscription;
  private subscription2: Subscription;
  private canClickNext: boolean;
  private currentPoints: number = 0;

  constructor(private addTripService: AddTripService, private trackService: TrackService,
              private activatedRoute: ActivatedRoute, private router: Router, private trackMapper: TrackMapper,
              private mountainRangeService: MountainRangeService) { }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe(
        (params: Params) => {
          this.mountainRangeId = +params['rangeId'];
          this.mountainGroupId = +params['groupId'];
          this.trackService.getTracksByMountainRangeId(this.mountainRangeId)
            .subscribe((tracks: Trasa[]) => {
              this.allTracks =  this.trackMapper.listFromJson(tracks);
              this.addTripService.setAllTracksListAndClearNewList(this.allTracks);
              this.mountainRangeName = tracks[0].pasmo.nazwa;
              this.mountainGroupName = tracks[0].pasmo.grupaGorska.nazwa;
            });
        }
      );
    this.subscription1 = this.addTripService.allTrackListSubject.subscribe(
      (allTracksList: Trasa[]) => this.allTracks = allTracksList
    );
    this.subscription2 = this.addTripService.newTrackListSubject.subscribe(
      (newTracksList: Trasa[]) => this.selectedTracks = newTracksList
    );
    this.addTripService.newTrackListSubject
      .subscribe((selectedTracksList: Trasa[]) => {
        selectedTracksList.length == 0 ? this.canClickNext = false : this.canClickNext = true;
      });

    const butt = document.getElementById('weryfikuj');
    butt.innerText = 'ZAPLANUJ WYCIECZKE';
    butt.classList.add('active');

    this.addTripService.currentPoints.subscribe((points: number) => {
      this.currentPoints = points;
    })
  }

  ngOnDestroy(): void {
    this.subscription1.unsubscribe();
    this.subscription2.unsubscribe();
  }

  onSubmit() {
    this.router.navigate(['/potwierdzNowaWycieczke'])
  }
}
