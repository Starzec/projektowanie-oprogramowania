import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plan-trip',
  templateUrl: './plan-trip.component.html',
  styleUrls: ['./plan-trip.component.css']
})
export class PlanTripComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    const butt = document.getElementById('weryfikuj');
    butt.innerText = 'ZAPLANUJ WYCIECZKE';
    butt.classList.add('active');
  }

}
