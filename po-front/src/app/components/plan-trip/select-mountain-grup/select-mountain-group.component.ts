import { Component, OnInit } from '@angular/core';
import {GrupaGorska} from '../../../models/GrupaGorska';
import {MountainGrupService} from '../../../models/services/MountainGrupService';

@Component({
  selector: 'app-select-mountain-grup',
  templateUrl: './select-mountain-group.component.html',
  styleUrls: ['./select-mountain-group.component.css']
})
export class SelectMountainGroupComponent implements OnInit {

  private mountainGroupList: GrupaGorska[];

  constructor(private mountainGroupService: MountainGrupService) { }

  ngOnInit() {
    this.mountainGroupService.getMountainGrupList();
    this.mountainGroupService.mountainGrupListSubject
      .subscribe((mountainGroupList: GrupaGorska[]) => {
        this.mountainGroupList = mountainGroupList;
      });
  }

}
