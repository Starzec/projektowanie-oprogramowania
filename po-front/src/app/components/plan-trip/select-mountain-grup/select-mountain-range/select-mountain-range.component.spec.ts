import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectMountainRangeComponent } from './select-mountain-range.component';

describe('SelectMountainRangeComponent', () => {
  let component: SelectMountainRangeComponent;
  let fixture: ComponentFixture<SelectMountainRangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectMountainRangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectMountainRangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
