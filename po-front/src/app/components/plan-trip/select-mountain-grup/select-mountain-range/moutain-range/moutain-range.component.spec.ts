import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoutainRangeComponent } from './moutain-range.component';

describe('MoutainRangeComponent', () => {
  let component: MoutainRangeComponent;
  let fixture: ComponentFixture<MoutainRangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoutainRangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoutainRangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
