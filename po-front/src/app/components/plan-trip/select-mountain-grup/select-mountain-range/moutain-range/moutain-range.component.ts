import {Component, Input, OnInit} from '@angular/core';
import {Pasmo} from '../../../../../models/Pasmo';
import {Router} from '@angular/router';

@Component({
  selector: 'app-moutain-range',
  templateUrl: './moutain-range.component.html',
  styleUrls: ['./moutain-range.component.css']
})
export class MoutainRangeComponent implements OnInit {

  @Input() mountainRange: Pasmo;
  @Input() mountainGroupId: number;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onClick() {
    this.router.navigate(["dodajWycieczke/" + this.mountainGroupId + '/' + this.mountainRange.pasmoId]);
  }

}
