import {Component, Input, OnInit} from '@angular/core';
import {Pasmo} from '../../../../models/Pasmo';
import {MountainRangeService} from '../../../../models/services/MountainRangeService';
import {ActivatedRoute, Params} from '@angular/router';
import {GrupaGorska} from "../../../../models/GrupaGorska";

@Component({
  selector: 'app-select-mountain-range',
  templateUrl: './select-mountain-range.component.html',
  styleUrls: ['./select-mountain-range.component.css']
})
export class SelectMountainRangeComponent implements OnInit {

  private mountainRangeList: Pasmo[];
  private mountainGroupId: number;

  constructor(private mountainRangeService: MountainRangeService,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe(
        (params: Params) => {
          this.mountainGroupId = +params['id'];

          this.mountainRangeService.getMountainRangeListForMountainGrup(this.mountainGroupId);
          this.mountainRangeService.mountainRangeListSubject
            .subscribe((mountainRangeList: Pasmo[]) => {
              this.mountainRangeList = mountainRangeList;
            });

        }
      );
  }

}
