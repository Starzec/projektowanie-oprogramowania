import {Component, Input, OnInit} from '@angular/core';
import {GrupaGorska} from '../../../../models/GrupaGorska';

@Component({
  selector: 'app-mountain-grup',
  templateUrl: './mountain-grup.component.html',
  styleUrls: ['./mountain-grup.component.css']
})
export class MountainGrupComponent implements OnInit {

  @Input() mountainGroup: GrupaGorska;

  constructor() { }

  ngOnInit() {
  }

}
