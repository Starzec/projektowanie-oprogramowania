import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MountainGrupComponent } from './mountain-grup.component';

describe('MountainGrupComponent', () => {
  let component: MountainGrupComponent;
  let fixture: ComponentFixture<MountainGrupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MountainGrupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MountainGrupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
