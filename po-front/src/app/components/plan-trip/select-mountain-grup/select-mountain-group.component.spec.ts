import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectMountainGroupComponent } from './select-mountain-group.component';

describe('SelectMountainGrupComponent', () => {
  let component: SelectMountainGroupComponent;
  let fixture: ComponentFixture<SelectMountainGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectMountainGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectMountainGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
