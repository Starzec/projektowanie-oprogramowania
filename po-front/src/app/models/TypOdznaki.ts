export class TypOdznaki {
  public typOdznakiId: number;
  public wymaganePunkty: number;
  public nazwa: string;
  public zdjecie: string;

  constructor(id: number, wymaganePunkty: number,
              nazwa:string, zdjecie: string){
    this.wymaganePunkty = wymaganePunkty;
    this.typOdznakiId = id;
    this.nazwa = nazwa;
    this.zdjecie = zdjecie;
  }
}
