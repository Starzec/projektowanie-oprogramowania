export class Uzytkownik {
  public userId: number;
  public imie: string;
  public nazwisko: string;
  public mail: string;
  public haslo: string;
  public zdjecie: string;

  constructor(id: number, imie: string,
              nazwisko: string, mail: string,
              haslo: string, zdjecie: string) {
    this.userId = id;
    this.imie = imie;
    this.nazwisko = nazwisko;
    this.mail = mail;
    this.haslo = haslo;
    this.zdjecie = zdjecie;
  }
}
