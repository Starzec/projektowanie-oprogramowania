import {Punkt} from './Punkt';
import {Trasa} from './Trasa';
import {Pasmo} from './Pasmo';
import {StatusTrasy} from './StatusTrasy';

export class TrasaZdefiniowana extends Trasa {

  public punktPoczatkowy: Punkt;
  public punktKoncowy: Punkt;
  public punkty: number;

  constructor(id: number, pasmo: Pasmo,
              statusTrasy: StatusTrasy,
              dataUtworzenia: string, nazwa: string,
              punktPoczatkowy: Punkt, punktKoncowy: Punkt, punkty: number, type: string) {
    super(id, pasmo, statusTrasy, dataUtworzenia, nazwa, type);
    this.punktKoncowy = punktKoncowy;
    this.punktPoczatkowy = punktPoczatkowy;
    this.punkty = punkty;
  }

  getPunkty(): number {
    return this.punkty;
  }

  getDescription(): string {
    return this.punktPoczatkowy.nazwa + ' -> ' +
      this.punktKoncowy.nazwa;
  }
}
