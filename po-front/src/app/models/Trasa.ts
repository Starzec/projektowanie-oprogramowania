import {StatusTrasy} from './StatusTrasy';
import {Pasmo} from './Pasmo';

export abstract class Trasa {
  public type: string;
  public trasaId: number;
  public pasmo: Pasmo;
  public statusTrasy: StatusTrasy;
  public dataUtworzenia: string;
  public nazwa: string;

  constructor(id: number, pasmo: Pasmo,
              statusTrasy: StatusTrasy,
              dataUtworzenia: string, nazwa: string)
  constructor(id: number, pasmo: Pasmo,
              statusTrasy: StatusTrasy,
              dataUtworzenia: string, nazwa: string, type: string)
  constructor(id: number, pasmo: Pasmo,
              statusTrasy: StatusTrasy,
              dataUtworzenia: string, nazwa: string, type?: string) {
    this.trasaId = id;
    this.pasmo = pasmo;
    this.statusTrasy = statusTrasy;
    this.dataUtworzenia = dataUtworzenia;
    this.nazwa = nazwa;
    this.type = type;
  }

  public abstract getPunkty(): number

  public abstract getDescription(): string
}


