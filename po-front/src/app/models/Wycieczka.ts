import {TypOdznaki} from './TypOdznaki';
import {Turysta} from './Turysta';
import {Trasa} from "./Trasa";

export class Wycieczka {
  public wycieczkaId: number;
  public typOdznaki: TypOdznaki;
  public turysta: Turysta;
  public dataWycieczki: string;
  public punktyZaWycieczke: number;
  public nazwa: string;
  public doWeryfikacji: boolean;
  public zweryfikowana: boolean;
  public trasaList: Trasa[];

  constructor(id: number, typOdznaki: TypOdznaki,
              turysta: Turysta, dataWycieczki: string,
              punktyZaWycieczke: number, nazwa: string,
              doWeryfikacji: boolean, zweryfikowana: boolean,
              trasy: Trasa[]) {
    this.wycieczkaId = id;
    this.typOdznaki = typOdznaki;
    this.turysta = turysta;
    this.dataWycieczki = dataWycieczki;
    this.punktyZaWycieczke = punktyZaWycieczke;
    this.nazwa = nazwa;
    this.doWeryfikacji = doWeryfikacji;
    this.zweryfikowana = zweryfikowana;
    this.trasaList = trasy;
  }
}
