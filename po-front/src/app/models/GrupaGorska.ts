export class GrupaGorska {
  public grupaGorskaId: number;
  public nazwa: string;

  constructor(id: number, nazwa: string) {
    this.grupaGorskaId = id;
    this.nazwa = nazwa;
  }
}
