import {Odznaka} from '../Odznaka';
import {Trasa} from '../Trasa';

export class CreateTripDto {
  public nazwa: string;
  public odznaka: Odznaka = null;
  public trasaList: Trasa[] = [];
  public date: Date;

  constructor(nazwa: string, listaTras: Trasa[], date: Date) {
    this.nazwa = nazwa;
    this.trasaList = listaTras;
    this.date = date;
  }
}
