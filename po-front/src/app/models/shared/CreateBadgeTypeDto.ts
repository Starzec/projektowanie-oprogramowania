export class CreateBadgeTypeDto {
  public wymaganePunkty: number;
  public nazwa: string;
  public zdjecie: string;

  constructor(wymaganePunkty: number,
              nazwa:string, zdjecie: string){
    this.wymaganePunkty = wymaganePunkty;
    this.nazwa = nazwa;
    this.zdjecie = zdjecie;
  }
}
