import {Uzytkownik} from './Uzytkownik';
import {Odznaka} from './Odznaka';

export class Turysta extends Uzytkownik {
  public dataUrodzenia: Date;

  constructor(id: number, imie: string,
              nazwisko: string, mail: string,
              haslo: string, zdjecie: string,
              dataUrodzenia:Date) {
    super(id, imie, nazwisko, mail, haslo, zdjecie);
    this.dataUrodzenia=dataUrodzenia;
  }
}
