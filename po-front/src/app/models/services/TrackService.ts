import {Injectable} from '@angular/core';
import {Trasa} from '../Trasa';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TrackService {

  constructor(private http: HttpClient) {}

  //
  getTracksByMountainRangeId(mountainRangeId: number) {
    return  this.http.get<Trasa[]>(environment.API_URL + '/track/byMountainRangeId',
      {params: {
          pasmoId: '' + mountainRangeId
        }} );
  }

  calculatePoints(distance: number, heightDifference: number) {
    var sum: number = 0;
    var kilo = Math.floor(distance/1000);
    var rest = distance - kilo*1000;
    sum = sum + kilo;
    if (rest > 500) {
      sum = sum + 1;
    }
    var fullDiff = Math.floor(heightDifference/100);
    var restDifference = heightDifference - fullDiff*100;
    sum = sum + fullDiff;
    if (restDifference > 50) {
      sum = sum + 1;
    }
    return sum;
  }
}
