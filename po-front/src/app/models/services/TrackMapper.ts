import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Trasa} from '../Trasa';
import {TrasaWlasna} from '../TrasaWlasna';
import {TrasaZdefiniowana} from '../TrasaZdefiniowana';

@Injectable({
  providedIn: 'root'
})
export class TrackMapper {

  constructor() {
  }

  objectFromJson(track: Trasa): Trasa {
    if (track.type == 'TrasaWlasna') {
      let trasaWlasna = track as TrasaWlasna;
      return new TrasaWlasna(trasaWlasna.trasaId, trasaWlasna.pasmo, trasaWlasna.statusTrasy, trasaWlasna.dataUtworzenia, trasaWlasna.nazwa,
        trasaWlasna.roznicaWysokosci, trasaWlasna.dlugosc, trasaWlasna.punktyZaPrzejscie, "TrasaWlasna");
    } else {
      let trasaZdef = track as TrasaZdefiniowana;
      return new TrasaZdefiniowana(trasaZdef.trasaId, trasaZdef.pasmo, trasaZdef.statusTrasy, trasaZdef.dataUtworzenia, trasaZdef.nazwa,
        trasaZdef.punktPoczatkowy, trasaZdef.punktKoncowy, trasaZdef.punkty, "TrasaZdefiniowana");
    }
  }

  listFromJson(list: Trasa[]): Trasa[] {
    let newList: Trasa[] = [];
    for (let track of list) {
      let trackObject = this.objectFromJson(track);
      newList.push(trackObject);
    }
    return newList;
  }

}
