import {Injectable} from '@angular/core';
import {Wycieczka} from '../Wycieczka';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TripService {

  verifiedTripsSubject = new Subject<void>();

  constructor(private http: HttpClient) {
  }

  //
  getTripsForVerification() {
    return this.http.get<Wycieczka[]>(environment.API_URL + '/trip-verify');
  }

  //
  getTripById(id: number) {
    return this.http.get<Wycieczka>(environment.API_URL + '/trip',
      {params: {
        wycieczkaId: '' + id
        }} );
  }

  //
  getTripsByTouristIdAndBadgeTypeId(turistId: number, badgeTypeId: number) {
    return this.http.get<Wycieczka[]>(environment.API_URL + '/trip/badge',
      {params: {
          typId: '' + badgeTypeId,
          turId: '' + turistId
        }} );
  }

  verifyTrip(tripId: number) {
    this.http.put(environment.API_URL + '/tour/accept', null,
      {params: {
          wycieczkaId: '' + tripId
        }}).subscribe( () =>
          this.verifiedTripsSubject.next()
    );
  }

  rejectTrip(tripId: number) {
    this.http.put(environment.API_URL + '/tour/reject', null,
      {params: {
          wycieczkaId: '' + tripId
        }}).subscribe(() =>
      this.verifiedTripsSubject.next());
  }

}
