import {Injectable} from '@angular/core';
import {Pasmo} from '../Pasmo';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MountainRangeService {

  private mountainRangeList: Pasmo[] = [];
  public mountainRangeListSubject = new Subject<Pasmo[]>();

  constructor(private http: HttpClient) {
  };

  getMountainRangeListForMountainGrup(mountainGrupId: number): Pasmo[] {
    this.fetchMountainRangeListForMountainGroup(mountainGrupId);
    return this.mountainRangeList;
  }

  getMountainRangeById(id: number) {
    for (let range of this.mountainRangeList) {
      if (range.pasmoId == id) {
        return range;
      }
    }

  }

  //
  private fetchMountainRangeListForMountainGroup(mountainGroupId: number) {
    this.http.get<Pasmo[]>(environment.API_URL + '/mountain-ranges',
      {
        params: {
          grupaId: '' + mountainGroupId
        }
      })
      .subscribe((mountainRanges: Pasmo[]) => {
        this.mountainRangeList = mountainRanges;
        this.mountainRangeListSubject.next(this.mountainRangeList)
      });
  }

}
