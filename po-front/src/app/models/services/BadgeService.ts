import {Injectable} from '@angular/core';
import {Odznaka} from '../Odznaka';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BadgeService {

  private turistBadges: Odznaka[];
  private currentBadge: Odznaka;
  turistBadgesSubject = new Subject<Odznaka[]>();
  currentBadgeSubject = new Subject<Odznaka>();

  constructor(private http: HttpClient) {
  }

  //
  getBadgesListForTurist(id: number) {
    this.http.get<Odznaka[]>(environment.API_URL + '/badges',
      {
        params: {
          turystaId: '' + id
        }
      }).subscribe((badges: Odznaka[]) => {
      this.turistBadges = [];
      for (let badge of badges) {
        if (!badge.czyZdobyta) {
          this.currentBadge = badge;
        } else {
          this.turistBadges.push(badge);
        }
      }
      this.turistBadgesSubject.next(this.turistBadges);
      this.currentBadgeSubject.next(this.currentBadge);
    });
  }

}
