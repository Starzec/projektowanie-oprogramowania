import {Trasa} from '../Trasa';
import {Subject} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Wycieczka} from '../Wycieczka';
import {environment} from '../../../environments/environment';
import {TrasaWlasna} from '../TrasaWlasna';
import {TrasaZdefiniowana} from '../TrasaZdefiniowana';
import {CreateTripDto} from '../shared/CreateTripDto';

@Injectable({
  providedIn: 'root'
})
export class AddTripService {

  private newTrackList: Trasa[] = [];
  private allTracksList: Trasa[] = [];

  public newTrackListSubject = new Subject<Trasa[]>();
  public allTrackListSubject = new Subject<Trasa[]>();
  public currentPoints = new Subject<number>();

  constructor(private http: HttpClient) {
   this.newTrackList = [];
    this.allTracksList = [];
  }

  saveTrip(name: string, date: Date): void {
    this.postTrip(name, date);
    this.clearLists();
  }

  postTrip(name: string, date: Date) {
    let trip: CreateTripDto = new CreateTripDto(name, this.newTrackList, date);
    this.http.post<Wycieczka[]>(environment.API_URL + '/tour', trip).subscribe();
  }

  objectFromJson(track: Trasa): Trasa {
    if (track.type == 'TrasaWlasna') {
      let trasaWlasna = track as TrasaWlasna;
      return new TrasaWlasna(trasaWlasna.trasaId, trasaWlasna.pasmo, trasaWlasna.statusTrasy, trasaWlasna.dataUtworzenia, trasaWlasna.nazwa,
        trasaWlasna.roznicaWysokosci, trasaWlasna.dlugosc, trasaWlasna.punktyZaPrzejscie, 'TrasaWlasna');
    } else {
      let trasaZdef = track as TrasaZdefiniowana;
      return new TrasaZdefiniowana(trasaZdef.trasaId, trasaZdef.pasmo, trasaZdef.statusTrasy, trasaZdef.dataUtworzenia, trasaZdef.nazwa,
        trasaZdef.punktPoczatkowy, trasaZdef.punktKoncowy, trasaZdef.punkty, 'TrasaZdefiniowana');
    }
  }

  moveToAnotherList(track: Trasa) {
    if (this.newTrackList.indexOf(track) !== -1) {
      this.addTrackToAllTracksList(track)
    } else {
      this.addTrackToNewList(track)
    }
    this.allTrackListSubject.next(this.allTracksList);
    this.newTrackListSubject.next(this.newTrackList);

    let currentPoints = this.getPointsFromNewList();
    this.currentPoints.next(currentPoints);
  }

  getPointsFromNewList() {
    var sum = 0;
    for (let track of this.newTrackList) {
      sum = sum + track.getPunkty();
    }
    return sum;
  }

  private addTrackToNewList(track: Trasa) {
    this.newTrackList.push(track);
    let trackIndex = this.allTracksList.indexOf(track);
    this.allTracksList = this.allTracksList.filter((trasa, index, array) => index !== trackIndex);
  }

  private addTrackToAllTracksList(track: Trasa) {
    this.allTracksList.push(track);
    let trackIndex = this.newTrackList.indexOf(track);
    this.newTrackList = this.newTrackList.filter((trasa, index, array) => index !== trackIndex);
  }

  setAllTracksListAndClearNewList(tracksList: Trasa[]) {
    this.allTracksList = tracksList;
    this.newTrackList = [];
    this.allTrackListSubject.next(this.allTracksList);
    this.newTrackListSubject.next(this.newTrackList);
  }

  clearLists() {
    this.allTracksList = [];
    this.newTrackList = [];
    this.allTrackListSubject.next(this.allTracksList);
    this.newTrackListSubject.next(this.newTrackList);
  }

  getNewTrackList() {
    return this.newTrackList;
  }

  calculatePoints() {
    var sum: number = 0;
    for (let track of this.newTrackList) {
      sum = sum + track.getPunkty();
    }
    return sum;
  }

  addToNewList(track: Trasa) {
    this.newTrackList.push(track);
    this.newTrackListSubject.next(this.newTrackList);
  }
}
