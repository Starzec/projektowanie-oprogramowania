import {Injectable} from '@angular/core';
import {TypOdznaki} from '../TypOdznaki';
import {Subject} from 'rxjs';
import {CreateBadgeTypeDto} from '../shared/CreateBadgeTypeDto';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BadgeTypeService {

  private typesList: TypOdznaki[] = [];

  typesListSubject: Subject<TypOdznaki[]> = new Subject<TypOdznaki[]>();

  constructor(private http: HttpClient) {
  }

  createNewBadgeType(newType: CreateBadgeTypeDto) {
    return this.http.post(environment.API_URL + '/badge-types', newType, {observe: 'response'});
  }

  getAllBadgeTypes(): TypOdznaki[] {
    this.fetchBadgeTypeList();
    return this.typesList;
  }

  //
  private fetchBadgeTypeList() {
    this.http.get<TypOdznaki[]>(environment.API_URL + '/badge-types')
      .subscribe((badgeTypes: TypOdznaki[]) => {
        this.typesList = badgeTypes;
        this.typesListSubject.next(this.typesList.slice());
      });
  }
}
