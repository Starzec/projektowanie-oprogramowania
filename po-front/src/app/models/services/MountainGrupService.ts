import {Injectable} from '@angular/core';
import {GrupaGorska} from '../GrupaGorska';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MountainGrupService {

  private mountainGrupList: GrupaGorska[] = [];
  public mountainGrupListSubject = new Subject<GrupaGorska[]>();

  constructor(private http: HttpClient){};

  getMountainGrupList(): GrupaGorska[] {
    this.fetchMountainGrouList();
    return this.mountainGrupList;
  }

  private fetchMountainGrouList() {
    this.http.get<GrupaGorska[]>(environment.API_URL + '/mountain-groups')
      .subscribe((mountainGroups: GrupaGorska[]) => {
        this.mountainGrupList = mountainGroups;
        this.mountainGrupListSubject.next(this.mountainGrupList);
      });
  }

}
