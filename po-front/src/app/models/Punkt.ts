export class Punkt {
  public id: number;
  public nazwa: string;
  public wysokoscNadPoziomemMorza: number;
  public szerokoscGeograficzna: number;
  public dlugoscGeograficzna: number;

  constructor(id: number, nazwa: string,
              wysokoscNadPoziomemMorza: number,
              szerokoscGeograficzna: number,
              dlugoscGeograficzna: number) {
    this.id = id;
    this.nazwa = nazwa;
    this.wysokoscNadPoziomemMorza = wysokoscNadPoziomemMorza;
    this.szerokoscGeograficzna = szerokoscGeograficzna;
    this.dlugoscGeograficzna = dlugoscGeograficzna;
  }
}
