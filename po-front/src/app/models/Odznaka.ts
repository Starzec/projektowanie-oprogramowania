import {TypOdznaki} from './TypOdznaki';
import {Turysta} from './Turysta';

export class Odznaka {
  public typOdznaki: TypOdznaki;
  public turysta: Turysta;
  public czyZdobyta: boolean;
  public rokZdobycia: Date;
  public aktualnePunkty: number;

  constructor(typOdznaki: TypOdznaki, turysta: Turysta,
              czyZdobyta: boolean, rokZdobycia: Date,
              aktualnePunkty: number) {
    this.typOdznaki = typOdznaki;
    this.czyZdobyta = czyZdobyta;
    this.turysta = turysta;
    this.rokZdobycia = rokZdobycia;
    this.aktualnePunkty = aktualnePunkty;
  }
}
