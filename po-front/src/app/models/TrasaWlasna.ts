import {Trasa} from './Trasa';
import {Pasmo} from './Pasmo';
import {StatusTrasy} from './StatusTrasy';

export class TrasaWlasna extends Trasa {
  public roznicaWysokosci: number;
  public dlugosc: number;
  public punktyZaPrzejscie: number;

  constructor(id: number, pasmo: Pasmo,
              statusTrasy: StatusTrasy,
              dataUtworzenia: string, nazwa: string,
              roznicaWysokosci: number,
              dlugosc: number, punktyZaPrzejscie: number, type: string) {
    super(id, pasmo, statusTrasy, dataUtworzenia, nazwa, type);
    this.roznicaWysokosci = roznicaWysokosci;
    this.dlugosc = dlugosc;
    this.punktyZaPrzejscie = punktyZaPrzejscie;
  }

  getPunkty(): number {
    return this.punktyZaPrzejscie;
  }

  getDescription(): string {
    return "Nazwa: " + this.nazwa + ' Długość: ' + this.dlugosc +
      ' Różnica Wysokości:' + this.roznicaWysokosci;
  }
}
