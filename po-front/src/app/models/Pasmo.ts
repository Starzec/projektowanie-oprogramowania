import {GrupaGorska} from './GrupaGorska';

export class Pasmo {
  public pasmoId: number;
  public grupaGorska: GrupaGorska;
  public nazwa: string;

  constructor(id: number, grupaGorska: GrupaGorska,
              nazwa: string) {
    this.pasmoId = id;
    this.grupaGorska = grupaGorska;
    this.nazwa = nazwa;
  }
}
